module.exports = () => ({
	apiUrl: "https://api.stembord.com",
	appUrl: "https://stembord.com/#",
	availableLocales: ["en", "de"]
});
