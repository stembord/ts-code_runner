module.exports = () => ({
	apiUrl: "http://localhost:4000",
	appUrl: "http://localhost:8000/#",
	availableLocales: ["en", "de"]
});
