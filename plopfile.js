module.exports = function(plop) {
    plop.setGenerator("component", {
        description: "React Component",
        prompts: [
            {
                type: "input",
                name: "name",
                message: "component name"
            },
            {
                type: "input",
                name: "scope",
                default: "",
                message: "scope for component"
            },
            {
                type: "confirm",
                name: "container",
                default: true,
                message: "does the component need a container"
            }
        ],
        actions: data => {
            const actions = [
                {
                    type: "add",
                    path: "lib/components/{{scope}}/{{name}}/{{name}}.tsx",
                    templateFile: "plop-templates/Component/Component.tsx.hbs"
                }
            ];

            if (data.container) {
                actions.push({
                    type: "add",
                    path: "lib/components/{{scope}}/{{name}}/index.ts",
                    templateFile:
                        "plop-templates/Component/export_Container.ts.hbs"
                });
                actions.push({
                    type: "add",
                    path:
                        "lib/components/{{scope}}/{{name}}/{{name}}Container.ts",
                    templateFile: "plop-templates/Component/Container.ts.hbs"
                });
            } else {
                actions.push({
                    type: "add",
                    path: "lib/components/{{scope}}/{{name}}/index.ts",
                    templateFile:
                        "plop-templates/Component/export_Component.ts.hbs"
                });
            }

            return actions;
        }
    });

    plop.setGenerator("page", {
        description: "Page React Component",
        prompts: [
            {
                type: "input",
                name: "name",
                message: "page component name"
            },
            {
                type: "input",
                name: "component",
                message: "component that page will load"
            },
            {
                type: "input",
                name: "path",
                message: "path for router"
            },
            {
                type: "input",
                name: "route state",
                message: "state name for route"
            }
        ],
        actions: [
            {
                type: "add",
                path: "lib/components/pages/{{name}}/{{name}}.tsx",
                templateFile: "plop-templates/Component/Page.tsx.hbs"
            },
            {
                type: "add",
                path: "lib/components/pages/{{name}}/index.ts",
                templateFile: "plop-templates/Component/export_Component.ts.hbs"
            },
            {
                type: "modify",
                path: "lib/routes.ts",
                pattern: /^/,
                template:
                    'import { {{name}} } from "./components/pages/{{name}}";\n'
            },
            {
                type: "append",
                path: "lib/routes.ts",
                template: 'route("{{state}}", "{{path}}", {{name}});'
            }
        ]
    });

    plop.setGenerator("store", {
        description: "Store",
        prompts: [
            {
                type: "input",
                name: "name",
                message: "store name"
            },
            {
                type: "input",
                name: "scope",
                default: "",
                message: "scope for store"
            }
        ],
        actions: [
            {
                type: "add",
                path: "lib/stores/{{scope}}/{{name}}.ts",
                templateFile: "plop-templates/store.ts.hbs"
            },
            {
                type: "modify",
                path: "lib/stores/index.ts",
                pattern: /^/,
                template: 'import * as {{name}} from "./{{name}}";\n'
            },
            {
                type: "append",
                path: "lib/stores/index.ts",
                template: "export { {{name}} };"
            }
        ]
    });
};
