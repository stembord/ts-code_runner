# ts-code_runner

code_runner

## plop

### component

```bash
$ plop component
? component name Example
? scope for component ()
? does the component need a container Yes
[SUCCESS] add /lib/components/Example/Example.tsx
[SUCCESS] add /lib/components/Example/index.ts
[SUCCESS] add /lib/components/Example/ExampleContainer.ts
```

### store

```bash
$ plop store
? store name example
? scope for store ()
[SUCCESS] add /lib/stores/example.ts
[SUCCESS] modify /lib/stores/index.ts
[SUCCESS] append /lib/stores/index.ts
```
