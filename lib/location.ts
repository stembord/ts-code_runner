import * as url from "url";
import { Location, getUrlPathname } from "@stembord/location";

const html5Mode = true;

export interface ILocationContext {
    pathname: string;
    url: url.UrlWithParsedQuery;
    redirectUrl: url.UrlWithParsedQuery | null;
    [key: string]: any;
}

const handler = (url: url.UrlWithParsedQuery) => {
    const context: ILocationContext = {
        pathname: url.pathname || "/",
        redirectUrl: null,
        url: url
    };

    routerStore.set(context.url);

    return router.handle(context).then(
        () => {
            if (context.redirectUrl) {
                location.set(getUrlPathname(context.redirectUrl));
                return Promise.reject(null);
            } else {
                return Promise.resolve(context.url);
            }
        },
        error => {
            if (context.redirectUrl) {
                location.set(getUrlPathname(context.redirectUrl));
                return Promise.reject(error);
            } else if (error) {
                return Promise.reject(error);
            } else {
                return Promise.reject(null);
            }
        }
    );
};

export const location = new Location(window, { html5Mode, handler });

import { router } from "./router";
import { router as routerStore } from "./stores";
