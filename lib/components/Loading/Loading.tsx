import * as React from "react";
import CircularProgress from "@material-ui/core/CircularProgress";

export interface ILoadingProps {}
export interface ILoadingState {}

export class Loading extends React.PureComponent<ILoadingProps, ILoadingState> {
    constructor(props: ILoadingProps) {
        super(props);

        this.state = {};
    }
    render() {
        return (
            <div>
                <CircularProgress />
            </div>
        );
    }
}
