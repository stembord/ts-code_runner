import * as React from "react";

export interface IAsyncProps<T> {
    promise: Promise<T>;
    onSuccess(value: T): React.ReactNode;
    onError(error: Error): React.ReactNode;
    onPending(): React.ReactNode;
}
export interface IAsyncState<T> {
    promise?: Promise<void>;
    value?: T;
    error?: Error;
}

export class Async<T> extends React.PureComponent<
    IAsyncProps<T>,
    IAsyncState<T>
> {
    constructor(props: IAsyncProps<T>) {
        super(props);

        this.state = {
            promise: null,
            value: null,
            error: null
        };
    }
    createPromise() {
        this.setState({
            promise: this.props.promise
                .then(value => this.setState({ value }))
                .catch(error => this.setState({ error }))
        });
    }
    componentDidMount() {
        if (this.props.promise) {
            this.createPromise();
        }
    }
    componentDidUpdate(prev: IAsyncProps<T>) {
        if (prev.promise !== this.props.promise) {
            this.createPromise();
        }
    }
    render() {
        const { error, value } = this.state,
            { onSuccess, onError, onPending } = this.props;

        if (error) {
            return onError(error);
        } else if (value) {
            return onSuccess(value);
        } else {
            return onPending();
        }
    }
}
