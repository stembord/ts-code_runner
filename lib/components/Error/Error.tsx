import * as React from "react";

export interface IErrorProps {
    error: Error;
}
export interface IErrorState {}

export class Error extends React.PureComponent<IErrorProps, IErrorState> {
    constructor(props: IErrorProps) {
        super(props);

        this.state = {};
    }
    render() {
        return <div>{JSON.stringify(this.props.error)}</div>;
    }
}
