import { connect } from "../../state";
import { codeEditor } from "../../stores";
import { CodeEditor } from "./CodeEditor";
import { IState } from "@stembord/state";

export const CodeEditorContainer = connect((state: IState) => ({
    value: codeEditor.selectValue(state),
    mode: codeEditor.selectValue(state),
    onChangeValue: codeEditor.changeValue,
    onChangeMode: codeEditor.changeMode
}))(CodeEditor);
