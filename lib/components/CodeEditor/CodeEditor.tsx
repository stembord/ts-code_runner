import * as React from "react";
import { Editor } from "./Editor";
import { languages } from "./languages";
import Select from "@material-ui/core/Select";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";

const LINE_HEIGHT = 15.6;

export interface ICodeEditorProps {
    value: string;
    mode: string;
    onChangeValue: (value: string) => void;
    onChangeMode: (mode: string) => void;
}
export interface ICodeEditorState {
    height: number;
}

export class CodeEditor extends React.PureComponent<
    ICodeEditorProps,
    ICodeEditorState
> {
    editor: React.RefObject<Editor>;
    onChangeValue: (value: string) => void;
    onChangeMode: (e: any) => void;

    constructor(props: ICodeEditorProps) {
        super(props);

        this.editor = React.createRef();

        this.state = {
            height: this.getHeight(this.props.value)
        };

        this.onChangeValue = value => {
            this.props.onChangeValue(value);
            this.setState({ height: this.getHeight(value) });
        };
        this.onChangeMode = e => {
            this.props.onChangeMode(e.target.value);
        };
    }
    getHeight(value) {
        return value.split("\n").length * LINE_HEIGHT;
    }
    render() {
        const { value, mode } = this.props,
            { height } = this.state;

        return (
            <div>
                <FormControl>
                    <InputLabel>Language</InputLabel>
                    <Select value={mode} onChange={this.onChangeMode}>
                        {languages.map(language => (
                            <MenuItem key={language} value={language}>
                                {language}
                            </MenuItem>
                        ))}
                    </Select>
                </FormControl>
                <Editor
                    ref={this.editor}
                    mode={mode}
                    width="100%"
                    height={height + "px"}
                    value={value}
                    onChange={this.onChangeValue}
                />
            </div>
        );
    }
}
