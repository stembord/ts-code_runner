import React from "react";
import AceEditor from "react-ace";

import "brace";
import "brace/theme/github";
import "brace/mode/markdown";
import "brace/mode/javascript";
import "brace/mode/elixir";
import "brace/mode/java";
import "brace/mode/python";
import "brace/mode/xml";
import "brace/mode/ruby";
import "brace/mode/rust";
import "brace/mode/sass";
import "brace/mode/mysql";
import "brace/mode/json";
import "brace/mode/html";
import "brace/mode/handlebars";
import "brace/mode/golang";
import "brace/mode/csharp";
import "brace/mode/coffee";
import "brace/mode/css";
import "brace/mode/c_cpp";

export interface IEditorProps {
    fontSize: number;
    mode: string;
    theme: string;
    onChange: (value: string) => void;
    value: string;
    width: string;
    height: string;
}
export interface IEditorState {}

export class Editor extends React.PureComponent<IEditorProps, IEditorState> {
    static defaultProps = {
        fontSize: 13,
        mode: "javascript",
        theme: "github",
        onChange: () => {},
        editorProps: {
            $blockScrolling: true
        },
        value: ""
    };

    render() {
        return <AceEditor {...this.props} />;
    }
}
