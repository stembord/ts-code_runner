import * as React from "react";
import { Async } from "../../Async";
import { Error } from "../../Error";
import { Loading } from "../../Loading";

export const SignIn = (props: {}) => (
    <Async
        promise={import("../../CodeEditor")}
        onSuccess={({ CodeEditor }) => <CodeEditor />}
        onError={error => <Error error={error} />}
        onPending={() => <Loading />}
    />
);
