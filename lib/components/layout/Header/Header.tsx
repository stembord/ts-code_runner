import * as React from "react";
import { FormattedMessage } from "react-intl";

export interface IHeaderProps {}
export interface IHeaderState {}

export class Header extends React.PureComponent<IHeaderProps, IHeaderState> {
    constructor(props: IHeaderProps) {
        super(props);

        this.state = {};
    }
    render() {
        return (
            <div>
                <h1>
                    <FormattedMessage id="app.name" />
                </h1>
            </div>
        );
    }
}
