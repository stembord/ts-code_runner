import { connect } from "../../../state";
import { Header } from "./Header";
import { IState } from "@stembord/state";

export const HeaderContainer = connect((state: IState) => ({}))(Header);
