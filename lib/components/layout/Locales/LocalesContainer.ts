import { connect } from "../../../state";
import { locales } from "../../../stores";
import { Locales } from "./Locales";
import { IState } from "@stembord/state";

export const LocalesContainer = connect(
    (state: IState, props: React.Attributes) => ({
        loadMessages: locales.reloadLocale,
        locale: locales.selectLocale(state),
        messages: locales.selectMessages(state)
    })
)(Locales);
