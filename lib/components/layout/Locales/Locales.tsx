import * as React from "react";
import { IntlProvider } from "react-intl";
import { IMessages } from "../../../stores/locales";

export interface ILocalesProps {
    loadMessages(): void;
    locale: string;
    messages: IMessages;
}

export class Locales extends React.PureComponent<ILocalesProps> {
    componentDidMount() {
        this.props.loadMessages();
    }
    componentDidUpdate(prev: ILocalesProps) {
        const { locale, loadMessages } = this.props;

        if (prev.locale !== locale) {
            loadMessages();
        }
    }
    render() {
        const { locale, messages, children } = this.props;

        if (!messages) {
            return <div />;
        } else {
            return (
                <IntlProvider locale={locale} messages={messages}>
                    {children}
                </IntlProvider>
            );
        }
    }
}
