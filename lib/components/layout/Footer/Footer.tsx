import * as React from "react";
import { FormattedMessage } from "react-intl";

export interface IFooterProps {
    locale: string;
    locales: Array<string>;
    setLocale: (locale: string) => Promise<{}>;
}
export interface IFooterState {}

export class Footer extends React.PureComponent<IFooterProps, IFooterState> {
    constructor(props: IFooterProps) {
        super(props);

        this.state = {};
    }
    render() {
        const { locale, locales, setLocale } = this.props;

        return (
            <div>
                {locales.map(l => (
                    <button
                        key={l}
                        onClick={() => setLocale(l)}
                        style={locale === l ? { color: "#888" } : {}}
                    >
                        <FormattedMessage id={`app.locales.${l}`} />
                    </button>
                ))}
            </div>
        );
    }
}
