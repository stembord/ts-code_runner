import { connect } from "../../../state";
import { locales } from "../../../stores";
import { Footer } from "./Footer";
import { IState } from "@stembord/state";

export const FooterContainer = connect((state: IState) => ({
    locale: locales.selectLocale(state),
    locales: locales.supportLocales(),
    setLocale: locales.setLocale
}))(Footer);
