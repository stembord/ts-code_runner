import * as React from "react";
import { Header } from "../Header";
import { Footer } from "../Footer";
import { Locales } from "../Locales";

export interface ILayoutProps {}
export interface ILayoutState {}

export class Layout extends React.PureComponent<ILayoutProps, ILayoutState> {
    constructor(props: ILayoutProps) {
        super(props);

        this.state = {};
    }
    render() {
        return (
            <Locales>
                <div>
                    <Header />
                    {this.props.children}
                    <Footer />
                </div>
            </Locales>
        );
    }
}
