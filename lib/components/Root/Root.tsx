import * as React from "react";
import { IState } from "@stembord/state";
import { Provider, state } from "../../state";
import { getComponent } from "../../router";
import { selectState } from "../../stores/router";

export interface IRootProps {}
export interface IRootState {
    value: IState;
}

export class Root extends React.PureComponent<IRootProps, IRootState> {
    private _isUpdating: boolean;
    private _isMounted: boolean;
    private _onSetState: () => void;
    private _runSetState: () => void;

    constructor(props: IRootProps) {
        super(props);

        this._isUpdating = false;
        this._isMounted = false;

        this.state = {
            value: state.getState()
        };

        this._onSetState = () => {
            if (!this._isUpdating) {
                this._isUpdating = true;
                process.nextTick(this._runSetState);
            }
        };

        this._runSetState = () => {
            this._isUpdating = false;
            if (this._isMounted) {
                this.setState({ value: state.getState() });
            }
        };
    }

    componentDidMount() {
        this._isMounted = true;
        state.addListener("set-state", this._onSetState);
    }

    componentWillUnmount() {
        this._isMounted = false;
        state.removeListener("set-state", this._onSetState);
    }

    render() {
        const state = this.state.value,
            Component = getComponent(selectState(state));

        return <Provider value={state}>{Component && <Component />}</Provider>;
    }
}
