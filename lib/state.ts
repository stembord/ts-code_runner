import { State } from "@stembord/state";
import { fromJS } from "immutable";
import { createContext } from "@stembord/state-react";

export const state = new State();
export const { Provider, connect } = createContext(state.getState());

if (process.env.NODE_ENV !== "production") {
    if ((window as any).devToolsExtension) {
        const devTools = (window as any).devToolsExtension.connect();

        devTools.subscribe(message => {
            if (
                message.type === "DISPATCH" &&
                message.payload.type === "JUMP_TO_ACTION"
            ) {
                const nextState = {},
                    json = JSON.parse(message.state);

                Object.keys(json).reduce((nextState, key) => {
                    nextState[key] = fromJS(json[key]);
                    return nextState;
                }, nextState);

                state.setState(nextState);
            }
        });

        state.on("set-state-for", name => {
            devTools.send(
                {
                    type: name || "unknown",
                    payload: state.getStateFor(name)
                },
                state.getState()
            );
        });
    }
}
