import * as React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { addLocaleData } from "react-intl";
import en from "react-intl/locale-data/en";
import de from "react-intl/locale-data/de";
import { Root } from "./components/Root";
import { location } from "./location";

import "./routes.ts";

addLocaleData([...en, ...de]);

window.addEventListener("load", () => {
    location.init();
    render(<Root />, document.getElementById("root"));
});

if ((module as any).hot) {
    ((module as any).hot as any).accept(() => {
        const rootNode = document.getElementById("root");

        unmountComponentAtNode(rootNode);
        render(<Root />, rootNode);
    });
}
