import { Record } from "immutable";
import { state } from "../state";
import * as url from "url";
import { IState } from "@stembord/state";

export const Router = Record({
    state: null,
    nextUrl: null,
    url: null
});

export const store = state.createStore<Router>("router", Router({}));

export const selectState = ({ router }: IState) => router.get("state");

export const set = (url: url.UrlWithParsedQuery) => {
    store.updateState(state => state.set("nextUrl", url));
};

export const update = (routeState: string) => {
    store.updateState(state => {
        const url = state.get("nextUrl");
        return state.set("state", routeState).set("url", url);
    });
};
