import * as codeEditor from "./codeEditor";
import * as router from "./router";
import * as locales from "./locales";

export { locales };
export { router };
export { codeEditor };