import { IState } from "@stembord/state";
import { Map, Record, fromJS } from "immutable";
import { state } from "../state";

export interface IMessages {
    [key: string]: string;
}

const locales: { [key: string]: Promise<IMessages> } = {
    en: import("../locales/en.json"),
    de: import("../locales/de.json")
};

export const Locales = Record({
    locale: "en",
    messages: Map<string, string>()
});

export const store = state.createStore("locales", Locales({}));

export const supportLocales = (() => {
    const keys = Object.keys(locales);
    return () => keys;
})();

export const selectLocale = ({ locales }: IState) => locales.get("locale");
export const selectMessages = ({ locales }: IState) => {
    const messages = locales.get("messages").get(locales.get("locale"));

    if (messages) {
        return messages.toJS();
    } else {
        return null;
    }
};

const LOADING = {};

export const setLocale = (nextLocale: string, force: boolean = false) => {
    const state = store.getState(),
        locale = state.get("locale"),
        messages = state.get("messages");

    if (!LOADING[nextLocale]) {
        if (!messages.has("nextLocale") || force) {
            LOADING[nextLocale] = true;

            return locales[nextLocale].then((localeMessages: IMessages) => {
                const msgs = fromJS(localeMessages);

                LOADING[nextLocale] = false;

                store.updateState(state =>
                    state
                        .set("locale", nextLocale)
                        .update("messages", messages =>
                            messages.set(nextLocale, msgs)
                        )
                );

                return msgs;
            });
        }
    }

    if (locale !== nextLocale) {
        store.updateState(state => state.set("locale", nextLocale));
    }

    return Promise.resolve(messages.get(nextLocale));
};

export const reloadLocale = (force: boolean = false) => {
    const state = store.getState();
    setLocale(state.get("locale"), force);
};

if ((module as any).hot) {
    ((module as any).hot as any).accept(() => {
        reloadLocale(true);
    });
}
