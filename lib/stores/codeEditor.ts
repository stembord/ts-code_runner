import { Map, Record, fromJS } from "immutable";
import { state } from "../state";
import { IState } from "@stembord/state";

export const CodeEditor = Record({
    value: "\n",
    mode: "javascript"
});

export const store = state.createStore<CodeEditor>(
    "codeEditor",
    CodeEditor({})
);

export const selectValue = ({ codeEditor }: IState) => codeEditor.get("value");
export const selectMode = ({ codeEditor }: IState) => codeEditor.get("mode");

export const changeValue = (value: string) =>
    store.updateState(state => state.set("value", value));

export const changeMode = (mode: string) =>
    store.updateState(state => state.set("mode", mode));
