import { SignIn } from "./components/pages/SignIn";
import { Home } from "./components/pages/Home";
import { route } from "./router";

route("index", "/", Home);
route("sign_in", "/sign_in", SignIn);
