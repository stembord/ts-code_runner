import * as React from "react";
import { Router } from "@stembord/router";
import { router as routerStore } from "./stores";

const COMPONENTS = {};

export const router = new Router();

export const route = (
    state: string,
    path: string,
    Component: React.ComponentType
) => {
    COMPONENTS[state] = Component;

    router.route(path, ctx => {
        routerStore.update(state);
        ctx.end();
    });
};

export const getComponent = (state: string) => COMPONENTS[state];
