const gulp = require("gulp"),
    ParcelBundler = require("parcel-bundler"),
    express = require("express"),
    del = require("del"),
    log = require("fancy-log"),
    configBundler = require("@nathanfaucett/config-bundler"),
    localesBundler = require("@nathanfaucett/locales-bundler");

const IS_PROD = process.env.NODE_ENV === "production",
    IS_TEST = process.env.NODE_ENV === "test",
    IS_DEV = !IS_PROD,
    SERVER_PORT = IS_TEST ? 8001 : 8000;

const cleanCache = () => del(["./.cache"]);

gulp.task("clean-cache", cleanCache);

const clean = () => del(["./lib/config.json", "./build"]);

gulp.task("clean", clean);

const config = () => {
    return gulp
        .src(["./config/*.js"])
        .pipe(configBundler())
        .pipe(gulp.dest("./lib"));
};

gulp.task("config", config);

const locales = () => {
    return gulp
        .src("./config/locales/**/*.json")
        .pipe(localesBundler({ flatten: true, minify: IS_PROD }))
        .pipe(gulp.dest("./lib/locales"));
};

gulp.task("locales", locales);

const parcel = watch => () => {
    const bundler = new ParcelBundler("./lib/index.html", {
        outDir: "./build",
        outFile: "index.html",
        publicUrl: "./",

        watch: watch === false ? false : IS_DEV,
        cache: IS_DEV,
        cacheDir: ".cache",
        contentHash: IS_PROD,

        minify: IS_PROD,
        target: "browser",
        https: IS_PROD,

        logLevel: 3,
        hmrPort: 0,
        sourceMaps: true,
        hmrHostname: "",
        detailedReport: false
    });

    return bundler.bundle();
};

gulp.task("parcel", parcel());

const serve = () => {
    const app = express();
    app.use(express.static("build"));
    app.listen(SERVER_PORT);
    log("Listening on port " + SERVER_PORT);
};

gulp.task("serve", serve);

const build = IS_PROD
    ? gulp.series(
          cleanCache,
          clean,
          gulp.parallel(config, locales),
          parcel(false)
      )
    : gulp.series(clean, gulp.parallel(config, locales), parcel(false));

gulp.task("build", build);

const run = IS_PROD
    ? gulp.series(cleanCache, clean, gulp.parallel(config, locales), parcel())
    : gulp.series(
          clean,
          gulp.parallel(config, locales),
          parcel(),
          gulp.parallel(serve)
      );

gulp.task("run", run);

gulp.task("default", run);
